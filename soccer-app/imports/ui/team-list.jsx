import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';
import { ActionDeleteForever } from 'material-ui/svg-icons/index';
import { red500 } from 'material-ui/styles/colors';

export default class TeamList extends Component {
  updateCurrentPlayer(player) {
    this.props.updateCurrentPlayer(player);
  }

  deletePlayer(playerId) {
    Meteor.call('deletePlayer', playerId, (error) => {
      if (error) {
        alert('Oops, something went wrong: ' + error.reason);
      }
      else {
        console.log('Player deleted, nothing to see here.');
      }
    });
  }

  render() {
    return (
      <ListItem
        primaryText={this.props.player.name}
        leftAvatar={<Avatar src="player.jpg"/>}
        rightIcon={<ActionDeleteForever
          onClick={this.deletePlayer.bind(this, this.props.player._id)}
          hoverColor={red500}
        />}
        onClick={this.updateCurrentPlayer.bind(this, this.props.player)}
        />
    )
  }
}
